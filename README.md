# Plutus

Cryptocurrency explorer. Takes key information and derives public keys if
required and then checks the matching blockchains for an active balance.

## Usage

```
plutus [-k key_file] [-c config_file]
```

Default key_file is `keys` default config_file is `config` if one does not exist
it will prompt the user for the information required to generate one.
