package main

import (
	"testing"
	"fmt"
)

func TestETHPub(t *testing.T){
	// raw
	got_raw := BuildKeys("0x01bE3584Aa54Edec052A13B287442c070123d4dD")

	want := []Key{Key {"0x01bE3584Aa54Edec052A13B287442c070123d4dD","","ETH"}}

	for i, v := range want {
		if v != got_raw[i] {
			t.Errorf("got %q, wanted %q", got_raw[i], v)
		}
	}

	/* t.Errorf("Write tagged tests")

	// tagged
	got_tagged_0 := BuildKeys("eth:pk:0x01bE3584Aa54Edec052A13B287442c070123d4dD")
	got_tagged_1 := BuildKeys("eth::0x01bE3584Aa54Edec052A13B287442c070123d4dD")
	got_tagged_2 := BuildKeys(":pk:0x01bE3584Aa54Edec052A13B287442c070123d4dD") */
}

func TestETHSec(t *testing.T){
	// raw
	got_raw := BuildKeys("0xccd38b02f58e7a3a76ad43660147e932560937b4ff2a741027500bf10a9a063b")
	fmt.Println(got_raw)

	want := []Key{Key {"0x01bE3584Aa54Edec052A13B287442c070123d4dD","0xccd38b02f58e7a3a76ad43660147e932560937b4ff2a741027500bf10a9a063b","ETH"}}
	for i, v := range want {
		if v != got_raw[i] {
			t.Errorf("got %q, wanted %q", got_raw[i], v)
		}
	}

	/*
	t.Errorf("Write tagged tests")
	// tagged
	got_tagged_0 := BuildKeys("eth:sk:0xccd38b02f58e7a3a76ad43660147e932560937b4ff2a741027500bf10a9a063b")
	got_tagged_1 := BuildKeys("eth::0xccd38b02f58e7a3a76ad43660147e932560937b4ff2a741027500bf10a9a063b")
	got_tagged_2 := BuildKeys(":sk:0xccd38b02f58e7a3a76ad43660147e932560937b4ff2a741027500bf10a9a063b") */
}

func TestBTCPub(t *testing.T){
	// raw
	got_raw := BuildKeys("bc1q22nj0pk8j3gnv4sd2s7u7t6k85llv5vj3a8f5f")

	want := []Key{Key {"bc1q22nj0pk8j3gnv4sd2s7u7t6k85llv5vj3a8f5f","","BTC"}}
	for i, v := range want {
		if v != got_raw[i] {
			t.Errorf("got %q, wanted %q", got_raw[i], v)
		}
	}

	/*t.Errorf("Write tagged tests")
	// tagged
	got_tagged_0 := BuildKeys("btc:pk:bc1q22nj0pk8j3gnv4sd2s7u7t6k85llv5vj3a8f5f")
	got_tagged_1 := BuildKeys("btc::bc1q22nj0pk8j3gnv4sd2s7u7t6k85llv5vj3a8f5f")
	got_tagged_2 := BuildKeys(":pk:bc1q22nj0pk8j3gnv4sd2s7u7t6k85llv5vj3a8f5f") */
}

func TestBTCSec(t *testing.T){
	// raw
	got_raw := BuildKeys("5KYZyF4hvQELqp9iJFeDHtbPkXDCxWaqiGSNfDJQ534hkoppgmp");

	want := []Key{Key {"bc1q22nj0pk8j3gnv4sd2s7u7t6k85llv5vj3a8f5f","5KYZyF4hvQELqp9iJFeDHtbPkXDCxWaqiGSNfDJQ534hkoppgmp","BTC"}}
	for i, v := range want {
		if v != got_raw[i] {
			t.Errorf("got %q, wanted %q", got_raw[i], v)
		}
	}

	/* t.Errorf("Write tagged tests")
	// tagged
	got_tagged_0 := BuildKeys("btc:sk:5KYZyF4hvQELqp9iJFeDHtbPkXDCxWaqiGSNfDJQ534hkoppgmp");
	got_tagged_1 := BuildKeys("btc::5KYZyF4hvQELqp9iJFeDHtbPkXDCxWaqiGSNfDJQ534hkoppgmp");
	got_tagged_2 := BuildKeys(":sk:5KYZyF4hvQELqp9iJFeDHtbPkXDCxWaqiGSNfDJQ534hkoppgmp"); */
}

/* func TestPhrase(t *testing.T){
	got := BuildKeys("stuff middle skirt invite decide erupt endless deposit exclude october this resource split stock spring");

	want := Key{
		"0x476a5ebb27a051740d6043ffda9d2ed94c1764fa5e4bb185490a1d8ccc0c2293",
		"0xe4A75c423E031d287f54Cd5667BcB6DB343Dd3A3",
		"",
	}
	if want != got[0] {
		t.Errorf("got %q, wanted %q", got, want)
	}
} */
